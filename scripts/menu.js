const menuBtn = document.getElementById("menuBtn");
const menu = document.getElementById("menu");
const body = document.getElementById("body");
const navItems = document.querySelectorAll(".menu__navItem");

menuBtn.addEventListener("click", () => toggleMenu());

function toggleMenu() {
  menu.classList.toggle("menu_active");
  body.classList.toggle("body_noScroll");
}
